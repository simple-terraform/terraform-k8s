terraform {
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
      version = "1.33.2"
    }
  }
}

provider "hcloud" {
  # HCLOUD_TOKEN added to ENV
}

resource "hcloud_server" "worker-1" {
  count       = 3
  location    = "fsn1"
  name        = "worker1"
  image       = "ubuntu-20.04"
  server_type = "cx11"
  ssh_keys    = [hcloud_ssh_key.default.id]
  labels      = {
    Name      = "K8S-server"
    Project   = "Terraform"
  }
}

resource "hcloud_ssh_key" "default" {
  name       = "SSH Key"
  public_key = file("~/.ssh/id_rsa.pub")
}
